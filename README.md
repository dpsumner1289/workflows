# Tips & Tricks

These are some helpful tips, tricks, commands, etc that can speed along dev processes. We'll also keep config files in this repo, and whatever else we think of that can be reused for efficiency purposes.

## Table of Contents

- Git commands
- Git configs
- Package manager configs

### Git Commands

First, we have a list of git commands. We'll include these in the README, since they don't really belong anywhere else.

#### Repo Cloning

Currently we use Pantheon, which by default clones the entire WP installation to your local machine, which is not (typically speaking) ideal. We usually work within the theme, so for this, we'll utilize the Git feature "sparse-checkout".

You can put these inside of a shell script on your local machine as well, but here, we'll list out the chained commands.

First, let's create our directory and initialize it as a Git repo:

`mkdir <your-repo-name> && cd $_ && git init`

Next, we can add our source url (something along the lines of "ssh://codeserver.dev.lotsofnumbersandsymbols/~/repository.git"):

`git remote add <source-url>`

Set sparse checkout to true

`git config core.sparseCheckout true`

Now you can add directories that you would like to check out:

`echo "some/dir/" >> .git/info/sparse-checkout`

`echo "some/other/dir" >> .git/info/sparse-checkout`

And finally, let's pull down our desired files:

`git pull origin master`

That's it! You now have a lean (or "sparse") local clone of your repo.

### Git Configs

A good .gitignore file is essential when trying to keep repos readable and organized. Refer to the .gitignore file in this repo, or make your own at [gitignore.io](https://www.gitignore.io/).

All you have to do is add this file to your repo root before you add your files for commit.

### Package Manager Configs

All of these configurations were initialized with Yarn, as it's currently the most cutting edge and fastest package manager, however any Yarn command can be translated into NPM as well, so we'll include instructions to both for each configuration.

These configurations are all kept in the package.json in this repo. Since comments are not allowed in JSON files, we'll list the relevant commands here.

#### Running SASS

`yarn sass || npm run sass`
